import { getCountryData, getWeatherData } from "../services/countryService";


describe("Api call test", () => {
  test("should render api data",async () => {
    const res = await getCountryData('Bangladesh');
    expect(res).toBeDefined();
   });
   test("should render api data",async () => {
    const res = await getWeatherData('capital');
    expect(res).toBeDefined();
   });
})