import {render, screen } from "@testing-library/react";
import { Router } from "react-router-dom";
import CountryInfo from "../components/CountryInfo";
import {createMemoryHistory} from 'history';

describe("CountryInfo component testing", () => {

  test("should render <CountryInfo /> component", () => {
    const history = createMemoryHistory();
    render(
      <Router history={history}>
        <CountryInfo />
      </Router>
    );
    const countryInfo = screen.getByTestId("countryInfo");
    expect(countryInfo).toBeInTheDocument();
  });

});