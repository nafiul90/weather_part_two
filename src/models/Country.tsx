export interface Country{
    capital: string[];
    population: number;
    latlng: number[];
    flags: {
        svg: string;
    };
}