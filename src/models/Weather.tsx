export interface Weather{
    temperature: number;
    weather_icons: string[];
    wind_speed: number;
    precip: number;
}