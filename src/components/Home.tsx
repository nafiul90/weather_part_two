import React, { useState } from 'react';
import {Button, TextField} from '@mui/material';
import { useHistory } from 'react-router-dom';

const Home = () => {
    const history = useHistory();
  
    const [countryName, setCountryName] = useState<string>();
  
    const handleInput = (e: React.ChangeEvent<HTMLInputElement>) => {
      setCountryName(e.target.value);
    };
  
    const handleSubmit = () => {
      history.push(`/country/${countryName}`);
    };
    return (
      <div
        data-testid="home"
        style={{
          display: "grid",
          justifyContent: "center",
          alignContent: "center",
          height: "50vh",
          width: "100%",
          gap: "20px",
        }}
      >
        <h2>Country information</h2>
        <TextField
          value={countryName}
          placeholder="Enter Country Name"
          onChange={handleInput}
          data-testid="name"
        />
        <Button
          variant="contained"
          color="info"
          disabled={!countryName}
          onClick={handleSubmit}
        >
          Get Details
        </Button>
      </div>
    );
  };
export default Home;