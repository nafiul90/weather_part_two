import { GET_COUNTRY_DATA, GET_WEATHER_DATA } from "../helpers/Constant"

export const getCountryData = (name: string) => {
    return fetch(`${GET_COUNTRY_DATA}/${name}`).then((res) => res.json());
}


export const getWeatherData = (capital: string | undefined) => {
    return fetch(`${GET_WEATHER_DATA}/${capital}`).then((res) => res.json());
}